
## About

The purpose of this webservice is to check the functionality, reliability and performance API using go. Using primary or most-commonly-used HTTP verbs POST, GET, PUT, and DELETE. These correspond to create, read, update, and delete operations, respectively and using a collection of first and last name of a user. Using go and its built in libraries, I was able to create a simple webservice to process a data user collection.


<!-- GETTING STARTED -->
## Getting Started

Download the project and build it. Default port is 8088. Test it with Postman. Send a post to /users with first and last name as json. Get the id with /user/:id
put with the id of the user to update the collection, also send a delete with the id to delete from the collection.

### Prerequisites

Postman to better test it.


<!-- LICENSE -->
## License

Distributed under the MIT License. 

<p align="right">(<a href="#top">back to top</a>)</p>



<!-- CONTACT -->
## Contact

Gregory Sosa - (https://www.linkedin.com/in/gregorysosa) - gregorysosa@gmail.com

