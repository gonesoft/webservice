package main

import (
	"net/http"

	controller "gitlab.com/gonesoft/webservice/Controllers"
)

func main() {
	controller.RegisterController()
	http.ListenAndServe(":8088", nil)
}
